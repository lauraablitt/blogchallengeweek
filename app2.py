from flask import Flask, render_template, request, redirect, url_for, redirect, session
from flask_sqlalchemy import SQLAlchemy

from wtforms import Form, StringField, SelectField

from datetime import datetime
import os





from flask import flash

import functools
import re
import urllib



app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/waitee/Documents/blogchallengeweek/blog.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=True
app.config['DEBUG']=True


APP_DIR = os.path.dirname(os.path.realpath(__file__))
DATABASE = 'sqliteext:///%s' % os.path.join(APP_DIR, 'blog.db')
DEBUG = False
SITE_WIDTH = 800
app.secret_key = 'shhh, secret!'







db = SQLAlchemy(app)

class Blogpost(db.Model):
     id = db.Column(db.Integer, primary_key=True)
     title = db.Column(db.String(50))
     subtitle = db.Column(db.String(50))
     author = db.Column(db.String(20))
     date_posted = db.Column(db.DateTime)
     content = db.Column(db.Text)


class SearchForm(Form):
    choices = [('title', 'title'),
               ('author', 'author'),
               ('content', 'content')]
    select = SelectField('Search for:', choices=choices)
    search = StringField('')


class CommentsDataBase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(20))
    date_posted = db.Column(db.DateTime)
    comments = db.Column(db.Text)
    rel_comment_id = db.Column(db.Integer, db.ForeignKey('blogpost.id'))


@app.route('/', methods=['GET', 'POST'])
def index():
    posts = Blogpost.query.order_by(Blogpost.date_posted.desc()).all()

    search = SearchForm(request.form)
    if request.method == 'POST':
        return search_results(search)

    return render_template('index.html', posts=posts, form=search)




@app.route('/results')
def search_results(search):
    results = []
    search_string = search.data['search']

    if search.data['search'] == '':
        qry = db_session.query(Albums)
        results = qry.all()

    if not results:
        flash('No results found!')
        return redirect('/')
    else:
        # display results
        return render_template('results.html', results=results)


@app.route('/login', methods=['GET', 'POST'])
def login():
    #only checks for post (if there has been a form submission)
    next_url = request.args.get('next') or request.form.get('next')
    if request.method == 'POST' and request.form.get('password'):
        if request.form['password'] == 'python':
            session['logged_in'] = True

            return render_template('add.html', next_url=next_url)
    else:
        session['logged_in'] = False
        flash('Incorrect password.', 'danger')
    return render_template('login.html', next_url = next_url)


@app.route('/logout')
def logout():
    session['logged_in'] = False


    return render_template('logout.html')





@app.route('/about')
def about():
     return render_template('about.html')

@app.route('/archive')
def archive():
    posts = Blogpost.query.order_by(Blogpost.date_posted.desc()).all()

    return render_template('archive.html', posts=posts)

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Blogpost.query.filter_by(id=post_id).one()
    comment = CommentsDataBase.query.filter_by(rel_comment_id=post_id)
    return render_template('post.html', post=post, comment=comment,post_id=post_id)

@app.route('/add')
def add():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return render_template('add.html')

@app.route('/comment', methods=['POST'])
def comment():
    author = request.form['author']
    comments = request.form['comments']# request Comments on the pages
    rel_comment_id = request.form['post_id']

    comment = CommentsDataBase(author=author, comments=comments, date_posted=datetime.now(), rel_comment_id=rel_comment_id)

    db.session.add(comment)
    db.session.commit()

    return  redirect(url_for('index'))



@app.route('/addpost', methods=['POST'])
def addpost():
    title = request.form['title']
    subtitle = request.form['subtitle']
    author = request.form['author']
    content = request.form['content']

    post = Blogpost(title=title, subtitle=subtitle, author=author, content=content, date_posted=datetime.now())

    db.session.add(post)
    db.session.commit()

    return redirect(url_for('index'))




@app.route('/search', methods=['GET'])
def search():
    search = SearchForm(request.form)
    if request.method == 'GET':
        return search_results(search)

    return redirect(url_for('index'))



if __name__ == '__main__':
    app.run(debug=True)
